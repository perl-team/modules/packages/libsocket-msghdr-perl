Source: libsocket-msghdr-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Mike Gabriel <sunweaver@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13),
               perl-xs-dev,
               perl:native
Standards-Version: 4.6.1
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libsocket-msghdr-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libsocket-msghdr-perl.git
Homepage: https://metacpan.org/release/Socket-MsgHdr
Rules-Requires-Root: no

Package: libsocket-msghdr-perl
Architecture: any
Depends: ${misc:Depends},
         ${perl:Depends},
         ${shlibs:Depends}
Description: sendmsg, recvmsg and ancillary data operations
 Socket::MsgHdr provides advanced socket messaging operations via sendmsg and
 recvmsg. Like their C counterparts, these functions accept few parameters,
 instead stuffing a lot of information into a complex structure.
 .
 This structure describes the message sent or received (buf), the peer on the
 other end of the socket (name), and ancillary or so-called control
 information (cmsghdr). This ancillary data may be used for file descriptor
 passing, IPv6 operations, and a host of implemenation-specific extensions.
